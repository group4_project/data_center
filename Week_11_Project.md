### Project Management:

1.st Scope - Time - Budget.
2.nd Optimization

	• Aim & Expectations: What are the aims & expectations
	• Plan: The planning and forecasting activities
	• Process: The overall approach to all activities and project governance.
	• People: Including dynamics of how they collaborate and communicate
	• Power: Lines of authority, decision-makers, organograms, politics for implementation (Organisational diagram)

### Brainstorming:

	• Quantity over quality
	• Withholding criticism
	• Any ideas are welcome.
	• Combination and improvements.
	• The problem statement should generate ideas rather than judgement i.e. "no" analytical judgement required.
	
	Different brainstorming techniques:
	• Nominal group technique  - Anonymous collection of ideas, Vote for ideas afterwards.
	• Group passing technique - Each person writes down one idea on one sheet of paper each. Paper circulates, each person adding onto the idea on the paper they just received Stops when you get your original idea back.
	• Team idea mapping - Brainstorms individually, ideas are afterwards merged into one big idea-map
	• Directed brainstorming - Each person starts with one sheet of paper → writes down one idea → shuffles papers → elaborate on the idea you just received
	• Guided brainstorming - Perspective and time is established beforehand → Devided into groups or individuals → Participants encouraged to adopt different mind-sets. → Gathering ideas and evaluates them afterwards
	• Individual brainstorming - free writing, free speaking, word association and drawing a mind-map
	• Question brainstorming -  Instead of doing a solution based brainstorm, "Questorming" Asks more questions for possible solutions 

### Risk analysis

Types of Threats:
Structural Threats - IT infrastructure 
Technical Threats - Tech advances or failures
Human Threats - Human error or loss of key employees, cyber theft
Natural Threats - Weather or natural disasters

Prioritizing risks:
Impact: The impact of a threat
Likelihood: Corresponds with impact. How big is the impact compared to the likelihood of it happening? WW3 would have a huge impact, but is not likely. 
Action: Should we even act? Again, WW3. We can’t really do much. Access to a huge threat should be prioritized. Prioritize if a threat is worth further problem solving for your project.

Risk examples:
Fire: needs a system that can stop the fire without the equipment being damaged more.
Hacking: needs a firewall/it team that can hold out any attacks on their systems.
Power outage: having backup generators or other types of power for if the power goes out.
Hardware failure: often checking up on the equipment and changing out often so the hardware doesn’t die while being used.

Risk management for our project:
Prepare for problems so when they happen you are ready and have a “guide” on what to do.
Communication for our client to make sure their system doesn’t go boom 🎇.
internal and external stakeholders

### Project Plannig
The objective of a project plan is to define the approach to be used by the project team to deliver the intended project management scope of the project.

At a minimum, a project plan answers basic questions about the project:

    Why ? - What is the problem or value proposition addressed by the project? Why is it being sponsored?
    What? - What is the work that will be performed on the project? What are the major products/deliverables?
    Who ? - Who will be involved and what will be their responsibilities within the project? How will they be organized?
    When? - What is the project timeline and when will particularly meaningful points, referred to as milestones, be complete?