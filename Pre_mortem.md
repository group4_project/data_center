[] Pre-mortem 

[] System failure
[] Reasons for failure:
- Sensor malfunction ,
- Over heating/fire component,
- Mqtt connection,
- Power loss,
- Humidity/water issue inside the center,
- Human error,
- Bugs,
- Space (Database),
- Security,
- CPU performance issue,
- Requirements heavy, complex, ambitious,
- Web server malfunction

[] Work ethic 
- Distruption from internal or external factors,
- Miscommunication between the stakeholders,
- Low time and effort commitment from the developers,
- Poor communication,
- Limited ideas for development
	
