# Complete project details at https://RandomNerdTutorials.com/micropython-mqtt-publish-dht11-dht22-esp32-esp8266/

import BME280
import random
import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from machine import Pin, SoftI2C
esp.osdebug(None)
import gc
gc.collect()

ssid = 'Cheska_Laptop_hotspot'
password = 'Hello_world'
mqtt_server = '192.168.137.49'

client_id = ubinascii.hexlify(machine.unique_id())

### Sensor 1 
topic_pub_temp_1 = b'esp/bme280_1/temp'
topic_pub_humid_1 = b'esp/bme280_1/humid'
topic_pub_pres_1 = b'esp/bme280_1/pres'
i2c_1 = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)
bme_1 = BME280.BME280(i2c=i2c_1, address=119)

### Sensor 2
topic_pub_temp_2 = b'esp/bme280_2/temp'
topic_pub_humid_2 = b'esp/bme280_2/humid'
topic_pub_pres_2 = b'esp/bme280_2/pres'
i2c_2 = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)
bme_2 = BME280.BME280(i2c=i2c_2, address=118)



last_message = 0
message_interval = 5

station = network.WLAN(network.STA_IF)

station.active(True)
#station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')

def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server, keepalive=30)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()

try:
  client = connect_mqtt()
except OSError as e:
  restart_and_reconnect()
while True:
    try:
        if (time.time() - last_message) > message_interval:
            #Sensor 1
            temp_1 = bme_1.values[0]
            pres_1 = bme_1.values[1]
            hum_1 = bme_1.values[2]
            #Sensor 2
            temp_2 = bme_2.values[0]
            pres_2 = bme_2.values[1]
            hum_2 = bme_2.values[2]
            #Sensor 1 publish
            client.publish(topic_pub_temp_1, temp_1)
            client.publish(topic_pub_humid_1, hum_1)
            client.publish(topic_pub_pres_1, pres_1)
            #Sensor 2
            client.publish(topic_pub_temp_2, temp_2)
            client.publish(topic_pub_humid_2, hum_2)
            client.publish(topic_pub_pres_2, pres_2)
            last_message = time.time()
    except OSError as e:
        restart_and_reconnect()
