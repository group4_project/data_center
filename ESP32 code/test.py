# Complete project details at https://RandomNerdTutorials.com/micropython-mqtt-publish-dht11-dht22-esp32-esp8266/

import BME280
import random
import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from machine import Pin, SoftI2C
esp.osdebug(None)
import gc
gc.collect()

ssid = 'Cheska_Laptop_hotspot'
password = 'Hello_world'
mqtt_server = '192.168.137.49'

client_id = ubinascii.hexlify(machine.unique_id())

topic_pub_temp = b'esp/bme280_1/temp'
topic_pub_humid = b'esp/bme280_1/humid'
topic_pub_pres = b'esp/bme280_1/pres'

last_message = 0
message_interval = 5

station = network.WLAN(network.STA_IF)

station.active(True)
#station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')

i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)
bme = BME280.BME280(i2c=i2c, address=119)

def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server, keepalive=30)
  #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()

try:
  client = connect_mqtt()
except OSError as e:
  restart_and_reconnect()
while True:
    try:
        if (time.time() - last_message) > message_interval:
            temp = bme.values[0]
            pres = bme.values[1]
            hum = bme.values[2]
            client.publish(topic_pub_temp, temp)
            client.publish(topic_pub_humid, hum)
            client.publish(topic_pub_pres, pres)
            last_message = time.time()
    except OSError as e:
        restart_and_reconnect()

