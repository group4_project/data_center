### System Overview  (21-03-22)

## The design for the Data center IoT system

[Link to the system plan](https://gitlab.com/group4_project/data_center/-/blob/main/Datacenter_plan_edited.pdf)


*Sensors are the instruments that initiate the data gathering process from the target/desired environment.*

__BME280__
- Input readings
	* Temperature
	* Humidity
	* Pressure 
- There will be 3 sensors on each rack
	* Top
	* Middle
	* Bottom

*The sensors are going to be physically connected to ESP32, it will process the data to be send to RPi through the localhost network*

__ESP32__
- All sensors will be connected to a single ESP32
	* Cool Area (8 racks x 3 sensors)
	* Hot Area (8 racks x 3 sensors)
- MQTT is used to deliver readings to the Raspberry Pi over the local network.

__Raspberry Pi (RPi)__
- The readings from the ESP32 are received via Node-red MQTT.
- The gauges will be created to appear on the homepage and will be ready to receive data from sensor readings.
- Set ranges on the gauges.

| Temperature | Humidity | Pressure    |
| ----------- | -------- | ----------- |
| 20-30°C     | 50-70%   | 900-1100pHa |

__Web page__
- Display data from RPi via http (localhost)
